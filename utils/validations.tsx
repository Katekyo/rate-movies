import { size } from 'lodash';
import * as Yup from 'yup';

export default function configurateValidations() {
    Yup.addMethod(Yup.string, 'validateEmail', function () {
        return this.test('validate-Email', 'The email most be valid.', function (valor) {
            if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(valor!)) {
                return false;
            }
            return true
        })
    })
    Yup.addMethod(Yup.string, 'validateSize', function () {
        return this.test('validate-Size', 'You must enter a password of at least 6 characters.', function (valor) {
            if (size(valor) < 6) {
                return false;
            }
            return true
        })
    })
}

