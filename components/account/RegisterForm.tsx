import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import { Input, Button, Icon, Text } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import Loading from "../Loading";
import { createAspUsersDTO } from "../../screen/account/account.model";
import { ErrorMessage, Formik, FormikHelpers } from "formik";
import * as Yup from "yup";
import configurateValidations from "../../utils/validations";



//import { size } from "lodash";
// import { validateEmail } from "../../utils/helpers";
// import { registerUser } from "../../utils/actions";


configurateValidations()
const RegisterForm = (props: registerFormProps) => {
  const [showPassword, setShowPassword] = useState(false);


  const [loading, setLoading] = useState(false);

  const navigation = useNavigation();

  
  return (
    <View style={styles.form}>
      <Formik initialValues={props.modelo} onSubmit={props.onSubmit} validationSchema={Yup.object({
        name: Yup.string().required('Name required'),
        lastname: Yup.string().required('Last name required'),
        email: Yup.string().required('Email required').validateEmail(),
        password: Yup.string().required('Password required').validateSize(),
        confirmPassword: Yup.string().required('Confirm Password required').validateSize().oneOf([Yup.ref('password'), null], 'Passwords must match'),
      })} >
        {({ handleChange, handleSubmit, values }) => (
          <View>
            <Input onChangeText={handleChange("name")} containerStyle={styles.input}  placeholder="Enter your name..." defaultValue={values.name} />
            <ErrorMessage name="name" >{msn =><Text style={styles.errors}>{msn}</Text> }</ErrorMessage>

            <Input onChangeText={handleChange("lastname")} containerStyle={styles.input} placeholder="Enter your last name..."  defaultValue={values.lastname}/>
            <ErrorMessage name="lastname" >{msn =><Text style={styles.errors}>{msn}</Text> }</ErrorMessage>

            <Input onChangeText={handleChange("email")} containerStyle={styles.input} placeholder="Enter your email..." keyboardType="email-address" defaultValue={values.email}/>
            <ErrorMessage  name="email" >{msn =><Text style={styles.errors} >{msn}</Text> }</ErrorMessage>

            <Input containerStyle={styles.input}  placeholder="Enter your password..."  onChangeText={handleChange("password")} secureTextEntry={!showPassword}  defaultValue={values.password}
              rightIcon={ <Icon  type="material-community" name={showPassword ? "eye-off-outline" : "eye-outline"} iconStyle={styles.icon} onPress={() => setShowPassword(!showPassword)} /> } />
            <ErrorMessage  name="password" >{msn =><Text style={styles.errors} >{msn}</Text> }</ErrorMessage>

            <Input containerStyle={styles.input} placeholder="Confirm your password..."   onChangeText={handleChange("confirmPassword")} secureTextEntry={!showPassword} defaultValue={values.confirmPassword}
              rightIcon={ <Icon type="material-community" name={showPassword ? "eye-off-outline" : "eye-outline"} iconStyle={styles.icon} onPress={() => setShowPassword(!showPassword)} /> } />
            <ErrorMessage  name="confirmPassword" >{msn =><Text style={styles.errors} >{msn}</Text> }</ErrorMessage>

            <Button  title="Create" onPress={() => handleSubmit()} containerStyle={styles.btnContainer} buttonStyle={styles.btn} />
          </View>
        )}
      </Formik>
      <Loading isVisible={loading} text="Creating account..." />
    </View>
  );
};

const styles = StyleSheet.create({
  form: {
    marginTop: 30,
  },
  input: {
    width: "100%",
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
    alignSelf: "center",
  },
  btn: {
    backgroundColor: "#ec0c54",
  },
  icon: {
    color: "#c1c1c1",
  },
  errors: {
    color: 'red',
    marginLeft: 10,
    marginTop: 0,
  }
});

interface registerFormProps {
  modelo: createAspUsersDTO;
  onSubmit(
    valores: createAspUsersDTO,
    accion: FormikHelpers<createAspUsersDTO>
  ): void;
}

export default RegisterForm;
