import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { Divider } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const Login = () => {
  return (
    <KeyboardAwareScrollView>
      <Image
        source={require("../../assets/movie_logo.png")}
        resizeMode="contain"
        style={styles.image}
      />
      <CreateAccount />
      <Divider style={styles.divider} />
    </KeyboardAwareScrollView>
  );
};

const CreateAccount = () => {
    const navigation = useNavigation()

  return (
    <Text style={styles.register}
    onPress={() => navigation.navigate("register")}>
      Aun no tienes una cuenta?{" "}
      <Text style={styles.btnRegister}>Registrate</Text>
    </Text>
  );
}

const styles = StyleSheet.create({
  image: {
    height: 150,
    width: "100%",
    marginBottom: 20,
  },
  container: {
    marginHorizontal: 40,
  },
  divider: {
    backgroundColor: "#ec0c54",
    margin: 40,
  },
  register: {
    marginTop: 15,
    marginHorizontal: 10,
    alignSelf: "center",
  },
  btnRegister: {
    color: "#ec0c54",
  },
});

export default Login;
