import { useFocusEffect } from "@react-navigation/native";
import React, { useCallback, useEffect, useState } from "react";
import { View, Text } from "react-native";
import Loading from "../../components/Loading";
import { getCurrentUser } from "../../utils/actions";
import UserGuest from "./UserGuest";
import UserLogged from "./UserLogged";

const Account = () => {
  const [login, setLogin] = useState<any>(null);
  useFocusEffect(
    useCallback(() => {
      validateUser();
    }, [])
  );
  const validateUser = async () => {
    const user = await getCurrentUser();
    user ? setLogin(false) : setLogin(false);
  };

  if (login == null) {
    return <Loading isVisible={true} text="Cargando..." />;
  }
  return login ? <UserLogged /> : <UserGuest />;

};

export default Account;
