import React from 'react'
import { StyleSheet, Text, ScrollView, Image } from "react-native";
import { Button } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';


const UserGuest = () => {
    const navigation = useNavigation();
    return (
        <ScrollView centerContent style={styles.viewBody}>
        <Image
          source={require("../../assets/movie_logo.png")}
          resizeMode="contain"
          style={styles.image}
        />
        <Text style={styles.title}>Check your profile on Rate Movies</Text>
        <Text style={styles.description}>
            How would you describe your best movie? 
            Search and view the best movies in a simple way,
            vote for the one you liked the most and comment on your experience.
        </Text>
        <Button title="View your profile" buttonStyle={styles.button} onPress={() => navigation.navigate("login") } />
      </ScrollView>
  
    )
}

const styles = StyleSheet.create({
    viewBody: {
      marginHorizontal: 30,
    },
    image: {
      width: "100%",
      height: 300,
      marginBottom: 10,
    },
    title: {
      fontWeight: "bold",
      fontSize: 19,
      marginVertical: 10,
      textAlign: "center",
    },
    description: {
      marginBottom: 20,
      color: "#ec447c",
      textAlign: "justify",
    },
    button: {
        backgroundColor: "#ec0c54"
    }
});
    
export default UserGuest
