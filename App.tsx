import React, { useEffect } from 'react';
import Navigation from './navigations/Navigation';
import { createTablats } from './utils/actions';

export default function App() {
  useEffect(() => { createTablats();}, [])
  return (
    <Navigation />
  )
}
