import React from 'react'
import { BottomTabBarOptions, createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { Icon } from 'react-native-elements';


import MoviesStack from './MoviesStack';
import AccountStack from './AccountStack';
import SearchStack from './SearchStack';
import FavoritesStack from './FavoritesStack';

const Tab = createBottomTabNavigator();

const Navigation = () => {
    const screenOptions = (route: any, color: any) => {
        let iconName = '';
        switch (route.name) {
          case "movies":
            iconName = "movie-outline";
            break;
          case "favorites":
            iconName = "heart-outline";
            break;
          case "top-restaurants":
            iconName = "star-outline";
            break;
          case "search":
            iconName = "movie-search-outline";
            break;
          case "account":
            iconName = "home-outline";
            break;
        }
        return (
          <Icon type="material-community" name={iconName} size={22} color={color} />
        );
      };
      
    return (
        <NavigationContainer>
            <Tab.Navigator initialRouteName= "movies" 
                tabBarOptions={{ 
                    inactiveTintColor: "#ec7ca4", 
                    activeTintColor: "#ec0c54" 
                }}
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ color }) => screenOptions(route, color)
                })} 
            >
                <Tab.Screen name="movies" component={MoviesStack} options={{ title: "Movies" }}  />
                <Tab.Screen name="favorites" component={FavoritesStack} options={{ title: "Favorite" }}  />
                <Tab.Screen name="search" component={SearchStack} options={{ title: "Search" }}  />
                <Tab.Screen name="account" component={AccountStack} options={{ title: "Account" }}  />
            </Tab.Navigator>
        </NavigationContainer>
    )
}

export default Navigation
