import React from 'react'
import { View, Text } from 'react-native'

import { createStackNavigator } from '@react-navigation/stack'
import Search from '../screen/Search'

const Stack = createStackNavigator()

const SearchStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="search" component={Search} options={{ title: "Buscar" }} />
        </Stack.Navigator>
    )
}

export default SearchStack
